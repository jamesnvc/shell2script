:- module(output_shell_test, []).

:- use_module(library(plunit)).
:- use_module(output/shell, [write_shell_script/2]).

:- begin_tests(output_shell).

test(basic_command_output,
     [ true(Script == "#!/usr/bin/env bash

set -euo pipefail

cd /
sudo -u beep touch /etc/aoeu
cd /
sudo ufw allow 443
cd /
ls -l /etc > /tmp/zzz 2> /tmp/yyy
cd /
curl --verbose localhost 2> /tmp/xxx > /tmp/www
cd /
curl --verbose localhost 2>&1 > /tmp/beep
") ]) :-
    Changes = [
        create(["/etc/aoeu"], ctx{as: "beep", redirects: [], cwd: "/"}),
        firewall(["allow", "443"], ctx{as: "root", redirects: [], cwd: "/"}),
        output(["ls", "-l", "/etc"], ctx{redirects: [to("/tmp/zzz"), error("/tmp/yyy")], cwd: "/"}),
        output(["curl", "--verbose", "localhost"],
               ctx{redirects: [error("/tmp/xxx"), to("/tmp/www")], cwd: "/"}),
        output(["curl", "--verbose", "localhost"],
               ctx{redirects: [error(fd(1)), to("/tmp/beep")], cwd: "/"})
    ],
    with_output_to(
        string(Script),
        write_shell_script(current_output, Changes)
    ).

test(quoted_pipeline_output_1,
    [ true( Script == "#!/usr/bin/env bash

set -euo pipefail

cd /
ls -l | grep 'pl$' > PL_FILES
") ]) :-
    Changes = [
        pipeline([command(["ls", "-l"], []),
                  command(["grep", "'pl$'"], [to("PL_FILES")])],
                ctx{cwd: "/"})],
    with_output_to(
        string(Script),
        write_shell_script(current_output, Changes)
    ).

test(quoted_pipeline_output_2,
    [ true( Script == "#!/usr/bin/env bash

set -euo pipefail

cd /
ls -l | grep \"pl\" > PL_FILES
") ]) :-
    Changes = [
        pipeline([command(["ls", "-l"], []),
                  command(["grep", "\"pl\""], [to("PL_FILES")])],
                ctx{cwd: "/"})],
    with_output_to(
        string(Script),
        write_shell_script(current_output, Changes)
    ).

:- end_tests(output_shell).
