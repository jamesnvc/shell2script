:- module(commands_test, []).

:- use_module(library(plunit)).
:- use_module(commands).

:- use_module(library(assoc), [list_to_assoc/2]).

:- begin_tests(commands).

test(line_command_1,
     [ true(Commands == ["foo", "BAZ=FOO", "x123x", "z123foo 123z"]) ]) :-
    Vars = [var_set(`FOO`, u(`123`)),
            var_set(`QUUX`, d([0'f, 0'o, 0'o, 0' , var(`FOO`)]))],
    Line = [u(`foo`), u(`BAZ=FOO`), u([0'x, var(`FOO`), 0'x]),
            u([0'z, var(`FOO`), var(`QUUX`), 0'z])],
    empty_env(Env0),
    set_env_vars(Vars, Env0, Env1),
    line_command(Env1, Line, Commands-Commands).

test(line_command_2,
     [ true( NewPath-Command == "/home/james/bin:/bin"-[] ) ]) :-
    list_to_assoc(["PATH"-"/bin"], Env0),
    set_env_vars([var_set(`PATH`, d([47,104,111,109,101,47,106,97,109,101,115,
                                     47,98,105,110,58, var(`PATH`)]))],
                 Env0, Env1),
    line_command(Env1, [], Command-Command),
    env_get("PATH", Env1, NewPath).

test(line_command_3_tilde,
     [ true( Command-HistCommand ==
             [ExpectedFoo0, ExpectedBeep0]-[ExpectedFoo, ExpectedBeep]) ]) :-
    list_to_assoc(["PATH"-"/bin", "BEEP"-"beep"], Env0),
    commands:user_home(HomeDir),
    string_concat(HomeDir, "/bin/foo", ExpectedFoo0),
    format(string(ExpectedFoo), "\"~w\"", [ExpectedFoo0]),
    string_concat(HomeDir, "/tmp/beep", ExpectedBeep0),
    format(string(ExpectedBeep), "\"~w\"", [ExpectedBeep0]),
    line_command(Env0, [d(`~/bin/foo`), d([126, 47, 116, 109, 112, 47, var(`BEEP`)])],
                 Command-HistCommand).

test(line_command_4_tilde_single,
     [ true( Command-HistCommand ==
             ["~/bin/foo", ExpectedBeep0]-[ExpectedFoo, ExpectedBeep]) ]) :-
    list_to_assoc(["PATH"-"/bin", "BEEP"-"beep"], Env0),
    commands:user_home(HomeDir),
    ExpectedFoo = "'~/bin/foo'",
    string_concat(HomeDir, "/tmp/beep", ExpectedBeep0),
    format(string(ExpectedBeep), "\"~w\"", [ExpectedBeep0]),
    line_command(Env0, [s(`~/bin/foo`), d([126, 47, 116, 109, 112, 47, var(`BEEP`)])],
                 Command-HistCommand).

test(line_command_5_unquoted,
     [ true( Command == [ExpectedFoo, ExpectedBeep]) ]) :-
    list_to_assoc(["PATH"-"/bin", "BEEP"-"beep"], Env0),
    commands:user_home(HomeDir),
    string_concat(HomeDir, "/bin/foo", ExpectedFoo),
    string_concat(HomeDir, "/tmp/beep", ExpectedBeep),
    line_command(Env0, [u(`~/bin/foo`), u([126, 47, 116, 109, 112, 47, var(`BEEP`)])],
                 Command-Command).

:- end_tests(commands).
