:- module(output_ansible_test, []).

:- use_module(library(plunit)).
:- use_module(output/ansible).

:- begin_tests(output_ansible).

test(basic_command_output_1,
    [ true(Script == "---
- file:
    path: /tmp/foo.txt
    state: present
") ]) :-
    Changes = [
        create(["foo.txt"], ctx{cwd: "/tmp"})
    ],
    with_output_to(
        string(Script),
        write_ansible_playbook(current_output, Changes)
    ).

test(basic_command_output_2,
    [ true(Script == "---
- file:
    path: /tmp/foo.txt
    state: present
- become: true
  file:
    path: /tmp/bar.txt
    state: present
- become: true
  become_user: bloop
  file:
    path: '{{ item }}'
    state: present
  loop:
  - /tmp/baz.txt
  - /tmp/quux.txt
- file:
    path: '{{ item }}'
    state: absent
  loop:
  - /home/quux
  - /tmp/zzz
") ]) :-
    Changes = [
        create(["foo.txt"], ctx{cwd: "/tmp"}),
        create(["bar.txt"], ctx{cwd: "/tmp", as: "root"}),
        create(["baz.txt", "quux.txt"], ctx{cwd: "/tmp", as: "bloop"}),
        remove(["-r", "quux", "/tmp/zzz"], ctx{cwd: "/home"})
    ],
    with_output_to(
        string(Script),
        write_ansible_playbook(current_output, Changes)
    ).

test(helper_function_1,
     [ true( ExNames = ['/home/james/foo', "/tmp/bar", '/home/james/../baz',
                        '/home/james/quux/zip'] ) ]) :-
    ansible:arg_files(["foo", "/tmp/bar", "../baz", "quux/zip"],
                      "/home/james",
                      ExNames).

test(output_command_output1,
    [ true(Script == "---
- name: command to file
  shell:
    chdir: /tmp
    cmd: echo baz > beep.txt
- become: true
  become_user: bloop
  name: command to file
  shell:
    chdir: /tmp
    cmd: grep foo /beep/baap >> /var/beep.txt 2> quux
")]) :-
    Changes = [
        output(["echo", "baz"], ctx{cwd: "/tmp", redirects: [to("beep.txt")]}),
        output(["grep", "foo", "/beep/baap"],
               ctx{cwd: "/tmp",
                   as: "bloop",
                   redirects: [append("/var/beep.txt"),
                               error("quux")]})
    ],
    with_output_to(
        string(Script),
        write_ansible_playbook(current_output, Changes)
    ).

:- end_tests(output_ansible).
