:- module(executing_test, []).

:- use_module(library(assoc), [list_to_assoc/2]).
:- use_module(library(plunit)).
:- use_module(library(readutil), [read_file_to_string/3]).
:- use_module(executing).
:- use_module(parsing, [line//2]).
:- use_module(commands, [empty_env/1]).

:- begin_tests(executing).

test(expand_redirect_test,
     [ true( Redirects == [to('/tmp/foobar'), error(fd(out))]) ]) :-
    phrase(parsing:line(_Vars, command(_Words, Redirects0)),
           `echo aoeu > foobar 2>&1 \n`),
    empty_env(Env),
    maplist(executing:expand_redirect_name(ctx{env: Env, cwd: "/tmp"}),
            Redirects0,
            Redirects).

test(running_command_1,
     [ true( Content-Status == "hello\n"-exit(0) ) ]) :-
    list_to_assoc(["PATH"-"/bin:/usr/bin"], Env),
    tmp_file(test_out, TmpOutput),
    tmp_file(test_script, TmpScript),
    setup_call_cleanup(
        open(TmpScript, write, Stream, []),
        format(Stream, "#!/bin/bash~n~necho hello >&2~n", []),
        close(Stream)
    ),
    executing:run_command(
                  ["bash", TmpScript],
                  [to(TmpOutput), error(fd(1))],
                  ctx{env: Env, cwd: "/tmp"},
                  Status
              ),
    read_file_to_string(TmpOutput, Content, []).

test(running_command_2,
     [ true( Content-Output == "world\n"-"hello\n" ) ]) :-
    list_to_assoc(["PATH"-"/bin:/usr/bin"], Env),
    tmp_file(test_out, TmpOutput),
    tmp_file(test_script, TmpScript),
    tmp_file(test_stdout, TmpStdout),
    setup_call_cleanup(
        open(TmpScript, write, Stream, []),
        format(Stream, "#!/bin/bash~n~necho hello >&2~necho world", []),
        close(Stream)
    ),
    setup_call_cleanup(
        open(TmpStdout, write, OutStream, []),
        with_output_to(
            OutStream,
            executing:run_command(
                          ["bash", TmpScript],
                          [error(fd(1)), to(TmpOutput)],
                          ctx{env: Env, cwd: "/tmp"},
                          _Status)
        ),
        close(OutStream)
    ),
    read_file_to_string(TmpStdout, Output, []),
    read_file_to_string(TmpOutput, Content, []).

test(running_command_3,
     [ true( Content-ErrOutput == "hello\n"-"world\n" ) ]) :-
    list_to_assoc(["PATH"-"/bin:/usr/bin"], Env),
    tmp_file(test_out, TmpOutput),
    tmp_file(test_script, TmpScript),
    tmp_file(test_err, TmpErr),
    setup_call_cleanup(
        open(TmpScript, write, Stream, []),
        format(Stream, "#!/bin/bash~n~necho hello >&2~necho world", []),
        close(Stream)
    ),
    setup_call_cleanup(
        ( stream_property(PrevErr, alias(user_error)),
          open(TmpErr, write, ErrStream, []),
          set_stream(ErrStream, alias(user_error)) ),
        executing:run_command(
                      ["bash", TmpScript],
                      [to(fd(2)), error(TmpOutput)],
                      ctx{env: Env, cwd: "/tmp"},
                      _Status),
        ( close(ErrStream),
          set_stream(PrevErr, alias(user_error)) )
    ),
    read_file_to_string(TmpErr, ErrOutput, []),
    read_file_to_string(TmpOutput, Content, []).

test(run_pipeline_1,
     [ true( Status-Output == exit(0)-"URYYB\n" ) ]) :-
    list_to_assoc(["PATH"-"/bin:/usr/bin"], Env),
    tmp_file(test_stdout, TmpStdout),
    setup_call_cleanup(
        open(TmpStdout, write, OutStream, []),
        with_output_to(
            OutStream,
            executing:run_pipeline(
                          [command(["echo", "hello"], []),
                           command(["tr", "[a-z]", "[A-Z]"], []),
                           command(["tr", "[A-Z]", "[N-ZA-M]"], [])],
                          ctx{env: Env, cwd: "/tmp"},
                          Status)
        ),
        close(OutStream)
    ),
    read_file_to_string(TmpStdout, Output, []).

test(run_pipeline_2,
     [ true( Status-Output == exit(3)-"URYYB\n" ) ]) :-
    list_to_assoc(["PATH"-"/bin:/usr/bin"], Env),
    tmp_file(test_stdout, TmpStdout),
    setup_call_cleanup(
        open(TmpStdout, write, OutStream, []),
        with_output_to(
            OutStream,
            executing:run_pipeline(
                          [command(["echo", "hello"], []),
                           command(["tr", "[a-z]", "[A-Z]"], []),
                           command(["bash", "-c", "echo HELLO; exit 3"], []),
                           command(["tr", "[A-Z]", "[N-ZA-M]"], [])],
                          ctx{env: Env, cwd: "/tmp"},
                          Status)
        ),
        close(OutStream)
    ),
    read_file_to_string(TmpStdout, Output, []).

test(running_1,
     [ true( Output == "hello world\n" ) ]) :-
    retractall(history_changes(_)),
    tmp_file(test_stdout, TmpStdout),
    list_to_assoc(["PATH"-"/bin:/usr/bin"], Env),
    setup_call_cleanup(
        open(TmpStdout, write, OutStream, []),
        with_output_to(
            OutStream,
            executing:loop(`echo hello 'world'\nexit\n`,
                           ctx{env: Env, cwd: "/"},
                           Hist-Hist)
        ),
        close(OutStream)
    ),
    read_file_to_string(TmpStdout, Output, []).

test(running_2,
     [ true( Output-Hist ==
             "hello\n"-[
                 ["echo", "hello"]-ctx{env: Env,
                                       cwd: "/",
                                       redirects: [to(TmpFilePathStr)]}] ) ]) :-
    retractall(history_changes(_)),
    tmp_file(test_file, TmpFilePath),
    list_to_assoc(["PATH"-"/bin:/usr/bin"], Env),
    format(codes(Command), 'echo hello > "~s"~nexit~n', [TmpFilePath]),
    executing:loop(Command,
                   ctx{env: Env, cwd: "/"},
                   Hist-Hist),
    atom_string(TmpFilePath, TmpFilePathStr),
    read_file_to_string(TmpFilePath, Output, []).


test(running_3,
     [ true( Output == "hello\nfoo\n" ) ]) :-
    retractall(history_changes(_)),
    tmp_file(test_stdout, TmpStdout),
    list_to_assoc(["PATH"-"/bin:/usr/bin"], Env),
    setup_call_cleanup(
        open(TmpStdout, write, OutStream, []),
        with_output_to(
            OutStream,
            executing:loop(`printf "hello\nworld\nfoo" | grep 'o$'\nexit\n`,
                           ctx{env: Env, cwd: "/"},
                           Hist-Hist)
        ),
        close(OutStream)
    ),
    read_file_to_string(TmpStdout, Output, []).

:- end_tests(executing).
