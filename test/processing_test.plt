:- module(processing_test, []).

:- use_module(library(plunit)).
:- use_module(processing).

:- begin_tests(processing).

test(filter_mutating_1,
     [ true(Changes == [remove(["foo"], ctx{}),
                        output(["ls", "-lh"],
                               ctx{redirects: [to("foo")]}),
                        output(["grep", "foo", "bar"],
                              ctx{redirects: [error("bar")]}),
                        stream_edit(["-i", "c/Foo/Bar/g", "somefile"],
                                    ctx{as: "root"}),
                        output(["ls", "-lah"],
                               ctx{as: "beep",
                                   redirects: [to("burf")]}),
                        create(["zurf"], ctx{})
                       ])
     ]) :-
    process_history(
        [["ls"]-ctx{},
         ["rm", "foo"]-ctx{},
         ["ls", "-lh"]-ctx{redirects: [to("foo")]},
         ["wc"]-ctx{redirects: [from("foo")]},
         ["grep", "foo", "bar"]-ctx{redirects: [error("bar")]},
         ["curl", "https://foo.bar"]-ctx{redirects: [to(fd(2))]},
         ["sudo", "sed", "-i", "c/Foo/Bar/g", "somefile"]-ctx{},
         ["sudo", "-u", "beep", "ls", "-lah"]-ctx{redirects: [to("burf")]},
         ["touch", "zurf"]-ctx{}
        ],
        Changes).

test(mutating_pipelines,
     [ true( Changes == [pipeline([command(["ls"], []),
                                   command(["sort", "-u"], [to("sorted")])],
                                  ctx{})]) ]) :-
    process_history([pipeline([command(["ls"], []),
                               command(["sort", "-u"], [to("sorted")])],
                              ctx{})],
                   Changes).

:- end_tests(processing).
