:- module(tracing_test, []).

:- use_module(library(filesex), [relative_file_name/3]).
:- use_module(library(pure_input), [phrase_from_file/2]).
:- use_module(library(plunit)).
:- use_module(tracing).

:- begin_tests(tracing).

test(parse_open_1,
     [ true( Line == open("/etc/ld.so.cache", 3,
                          [flags([read_only, close_on_exec])])) ]) :-
    phrase(tracing:strace_line(Line),
           `7340  openat(AT_FDCWD, "/etc/ld.so.cache", O_RDONLY|O_CLOEXEC) = 3\n`).

test(parse_open_2,
     [ true( Line == open(".aaeou.swp", 4,
                          [flags([read_write, create, non_existing, no_follow]),
                           mode(`0600`)])) ]) :-
    phrase(tracing:strace_line(Line),
           `7340  openat(AT_FDCWD, ".aaeou.swp", O_RDWR|O_CREAT|O_EXCL|O_NOFOLLOW, 0600) = 4\n`).


test(parse_strace_output,
     [ true( FirstCall-FirstFd ==
             open("/etc/ld.so.cache", 3,
                  [flags([read_only, close_on_exec])])-1 ) ]) :-
    module_property(tracing_test, file(ThisFile)),
    relative_file_name(TestFile, ThisFile, "./strace_test_output"),
    phrase_from_file(
        tracing:strace_output(Lines-[]),
        TestFile),
    Lines = [FirstCall|_],
    memberchk(write(FirstFd), Lines).

test(relative_open,
     [ true(Traces1 ==
            [open('/home/james/tmp', 3, []),
             open('/home/james/tmp/beep/boop', 4, [])]) ]) :-
    Traces0 = [open(".", 3, []),
               open_rel(3, "beep/boop", 4, [])],
    tracing:canonicalize_path_names(
                "/home/james/tmp", Traces0, Traces1).

test(find_written_files,
     [ true(Written == ['/home/james/tmp/.foobar.swp',
                        '/home/james/tmp/foobar~',
                        '/home/james/tmp/foobar',
                        '/tmp/.bcdefg.swp',
                        '/tmp/bcdefg',
                        '/home/james/Documents/.foobar.swp',
                        '/home/james/Documents/foobar~',
                        '/home/james/Documents/foobar'
                        ]) ]) :-
    module_property(tracing_test, file(ThisFile)),
    relative_file_name(TestFile, ThisFile, "./strace_test_output"),
    phrase_from_file(
        tracing:strace_output(Lines0-[]),
        TestFile),
    tracing:canonicalize_path_names("/home/james/tmp", Lines0, Lines),
    tracing:written_to(Lines, Written).

test(find_written_files_dif_list,
     [ true(Written == ['/home/james/tmp/.foobar.swp',
                        '/home/james/tmp/foobar~',
                        '/home/james/tmp/foobar',
                        '/tmp/.bcdefg.swp',
                        '/tmp/bcdefg',
                        '/home/james/Documents/.foobar.swp',
                        '/home/james/Documents/foobar~',
                        '/home/james/Documents/foobar',
                        '/home/gurf']) ]) :-
    module_property(tracing_test, file(ThisFile)),
    relative_file_name(TestFile, ThisFile, "./strace_test_output"),
    phrase_from_file(
        tracing:strace_output(Lines0-Tail),
        TestFile),
    Tail = [open("/home/gurf", 3, [flags([read_write])]),
            write(3)],
    tracing:canonicalize_path_names("/home/james/tmp", Lines0, Lines),
    tracing:written_to(Lines, Written).



:- end_tests(tracing).
