:- module(parsing_test, []).

:- use_module(library(plunit)).
:- use_module(parsing).

:- begin_tests(parsing).

test(parsing_var_1, [ true(V == `Foo`) ]) :-
    phrase(parsing:var(V), `$Foo`).

test(parsing_var_1, [ true(V == `Foo`) ]) :-
    phrase(parsing:var(V), `${Foo}`).

test(parsing_var_2, [ fail ]) :-
    phrase(parsing:var(_), `$`).

test(parsing_single_quoted_1,
     [ true(W == s(`hello world`)) ]) :-
    phrase(parsing:word(W), `'hello world'`).

test(parsing_single_quoted_escaped,
     [ true(W == s(`hello y'all`)) ]) :-
    phrase(parsing:word(W), `'hello y\\'all'`).

test(parsing_unquoted,
     [ true(W == u(`hello`)) ]) :-
    phrase(parsing:word(W), `hello`).

test(parsing_unquoted_escpaed_space,
     [ true(W == u(`hello world`)) ]) :-
    phrase(parsing:word(W), `hello\\ world`).

test(parsing_double_string_1,
     [ true(Dq == d(`foo bar 'baz'`)) ]) :-
    phrase(parsing:word(Dq), `"foo bar 'baz'"`).

test(parsing_double_string_2,
     [ true(Dq == d([102, 111, 111, 32, 98, 97, 114, 32,
                     var(`BEEP`),
                     32, 39, 98, 97, 122, 39])) ]) :-
    phrase(parsing:word(Dq), `"foo bar $BEEP 'baz'"`).

test(parsing_double_string_3,
     [ true(Dq == d([102, 111, 111, 32, var(`2`), 32, 98, 97, 114, 32,
                     var(`BEEP`), 32, 39, 98, 97, 122,
                     var(`boop`), 39])) ]) :-
    phrase(parsing:word(Dq), `"foo $2 bar $BEEP 'baz${boop}'"`).

test(parsing_words_1,
     [ true(Vs-Ws == []-[s(`hello world`), u(`foo`)]) ]) :-
    phrase(parsing:line(Vs, command(Ws, _)), `'hello world'   foo  \n`).

test(parsing_words_2,
     [ true(Vs-Ws == []-[u(`foo`), s(`hello y'all`), u(`beep boop`)]) ]) :-
    phrase(parsing:line(Vs, command(Ws, _)), `foo 'hello y\\'all' beep\\ boop\n`).

test(parsing_words_2,
     [ true(Vs-Ws == []-[u(`foo`), u([var(`FOO`)]),
                         d([98, 101, 101, 112, 32, var(`baz`),
                            32, 98, 111, 111, 112])]) ]) :-
    phrase(parsing:line(Vs, command(Ws, _)), `foo $FOO "beep ${baz} boop"\n`).

test(parsing_words_3,
     [ true(Vs-Ws == [var_set(`FOO`, u(`123`)),
                      var_set(`QUUX`, d([102, 111, 111, 32, var(`FOO`)]))
                     ]-[u(`foo`),
                        u(`BAZ=FOO`),
                        u([0'x, var(`FOO`), 0'x]),
                        d([0'z, var(`FOO`), var(`QUUX`), 0'z])]) ]) :-
    phrase(parsing:line(Vs, command(Ws, _)), `FOO=123  QUUX="foo ${FOO}" foo BAZ=FOO x${FOO}x "z$FOO${QUUX}z"\n`).

test(parsing_words_4,
     [ true(Vs-Rs-Ws == []-[]-[u(`echo`), u([var(`PATH`)])]) ]) :-
    phrase(parsing:line(Vs, command(Ws, Rs)), `echo $PATH\n`).

test(parsing_words_5,
     [ true(Vs-Rs-Ws == []-[to(u(`foobar`))]-[u(`echo`), u([var(`PATH`)])]) ]) :-
    phrase(parsing:line(Vs, command(Ws, Rs)), `echo $PATH > foobar\n`).

test(parsing_words_6,
     [ true(Vs-Rs-Ws == []-[to(u(`foobar`)), from(u(`baz`))]-[u(`echo`), u([var(`PATH`)])]) ]) :-
    phrase(parsing:line(Vs, command(Ws, Rs)), `echo $PATH > foobar < baz\n`).

test(parsing_redirect_1,
     [ true(Redirects == [both(u(`beep`))]) ]) :-
    phrase(parsing:line(_Vars, command(_Words, Redirects)),
          `echo aoeu &> beep\n`).

test(parsing_redirect_2,
     [ true(Redirects == [to(u(`foobar`)), error(fd(u(`1`)))]) ]) :-
    phrase(parsing:line(_Vars, command(_Words, Redirects)),
           `echo aoeu > foobar 2>&1 \n`).

test(parsing_redirect_2,
     [ true(Redirects == [append(u(`aoeu`))]) ]) :-
    phrase(parsing:line(_Vars, command(_Words, Redirects)),
           `echo aoeu >>aoeu \n`).


test(parsing_pipes_1,
     [ true( Command == pipe(command([u(`echo`), u(`hello`)], []),
                            command([u(`sed`), s(`s/^h/H/g`)], [])) ) ]) :-
    phrase(parsing:line(_Vars, Command),
          `echo hello | sed 's/^h/H/g'\n`).

test(parsing_pipes_2,
     [ true( Command ==
             pipe(command([u(`curl`), u(`--verbose`), u(`https://www.google.com`)],
                          [error(fd(u(`1`))), to(u(`/dev/null`))]),
                  pipe(command([u(`grep`), u(`-i`), s(`^< date: `)],
                               []),
                       command([u(`sed`), s(`s/GMT/UTC/g`)], []))) ) ]) :-
    phrase(parsing:line(_Vars, Command),
          `curl --verbose https://www.google.com 2>&1 > /dev/null | grep -i '^< date: ' | sed 's/GMT/UTC/g'\n`).

:- end_tests(parsing).
