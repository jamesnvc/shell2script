:- module(executing, [main/0]).
/** <module> Top-level module for app

The module for actually reading & running commands.

@author James Cash
*/
:- use_module(library(apply_macros)).
:- use_module(library(assoc), [list_to_assoc/2]).
:- use_module(library(debug), [debug/1, debug/3]).
:- use_module(library(filesex), [directory_file_path/3]).
:- use_module(library(lists), [append/3, member/2]).
:- use_module(library(process), [process_create/3, process_wait/2,
                                 process_set_method/1]).
:- use_module(library(pure_input), [stream_to_lazy_list/2]).
:- use_module(library(yall)).
:- use_module(parsing, [line//2]).
:- use_module(commands, [line_command/3,
                         set_env_vars/3,
                         sh_string_val/3,
                         env_list/2,
                         env_get/3]).
:- use_module(line_bindings, [setup_shell_editline/0,
                              shell_history_add/1]).
:- use_module(tracing, [trace_editing_command/3]).
:- use_module(processing, [process_history/2,
                           sudo_command/4]).
:- use_module(output, [write_script/3]).
:- use_module(output/shell, [output_change/2 as shell_output_change]).

% Entry point

%! main is det.
%
%  Start the app running. Recieves a single argument from the command
%  line, the location that the output will be writen to.
main :-
    current_prolog_flag(argv, [OutputPath]), !,
    start(OutputPath).
main :-
    format(user_error, "Usage: bard <outputfile>~n", []).

main_dev :-
    debug(executing),
    debug(tracing),
    current_prolog_flag(argv, [OutputPath]), !,
    start(OutputPath).

start(OutputPath) :-
    setup_shell_editline,
    stream_to_lazy_list(user_input, List),
    getenv("PATH", Path),
    list_to_assoc(["PATH"-Path], Env),
    working_directory(Cwd, Cwd),
    loop(List, ctx{env: Env, cwd: Cwd}, H-H), !,
    process_history(H, Changes),
    debug(executing, "History ~w", [Changes]),
    setup_call_cleanup(
        open(OutputPath, write, Stream, []),
        write_script(ansible, Stream, Changes),
        close(Stream)
    ).

% Main loop

loop(In, Ctx0, History) :-
    phrase(line(Vars, Command), In, Rst),
    % Get the literal string entered, for history
    difflist_list(In, Rst, CmdCodes),
    string_codes(CmdStr, CmdCodes),
    shell_history_add(CmdStr),
    % process command
    set_env_vars(Vars, Ctx0.env, Env1),
    Ctx1 = Ctx0.put([env=Env1]),
    once(handle(Command, Ctx1, Rst, History)).

%! handle(+Command, +Context:dict, +Remainder, +Hist:list) is det.
%
%  Handle the entered =Command=, given =Context=.
%  =Remainder= is the rest of lazy list/stream the command is being
%  read from and =History= is a difference list (in the form
%  =Head=-=Tail=) of the commands that have been entered.
handle(command(Words, Redirects0), Ctx, Rst, History) :- !,
    line_command(Ctx.env, Words, Command-HistCommand),
    maplist(expand_redirect_name(Ctx), Redirects0, Redirects),
    debug(executing, "COMMAND ~w ~w", [Command, Redirects]),
    once(handle_single(Command-HistCommand, Redirects, Ctx, Rst, History)).
handle(pipe(Command1, CommandsNext), Ctx0, Rst, History-Tail0) :-
    debug(executing, "Piping ~w into ~w", [Command1, CommandsNext]),
    pipe_commands(pipe(Command1, CommandsNext), Commands0),
    maplist({Ctx0}/[command(Words, Redirs0), command(Cmd, Redirs), command(HistCmd, Redirs)]>>(
                line_command(Ctx0.env, Words, Cmd-HistCmd),
                maplist(expand_redirect_name(Ctx0), Redirs0, Redirs)
            ), Commands0, Commands, HistCommands),
    run_pipeline(Commands, Ctx0, Status),
    % [TODO] assuming that no editing in pipeline
    % [TODO] assuming no changes to context in the pipeline
    ( Status == exit(0)
    -> Tail0 = [pipeline(HistCommands, Ctx0)|Tail1]
    ;  Tail1 = Tail0),
    loop(Rst, Ctx0, History-Tail1).

%! pipe_commands(+Pipeline:term, -Commands:list) is det.
%
%  True when =Commands= is a list of the commands represented by the
%  nested compound term =Pipeline=.
pipe_commands(command(Command, Redirs), [command(Command, Redirs)]).
pipe_commands(pipe(Command, Next), [Command|NextCommands]) :-
    pipe_commands(Next, NextCommands).

% "exit" to quit the shell
handle_single(["exit"|_]-_, _, _, _, _-[]).
% ignore empty
handle_single([]-_, _, Ctx, In, H) :- loop(In, Ctx, H).
% cd changes cwd
handle_single(["cd", Location]-_, _, Ctx, In, H) :-
    loop(In, Ctx.put(cwd, Location), H).
% trace "editing" type commands (vi, nano, etc)
handle_single(CmdArgs-_, _Redirects, Ctx0, In, Hist-HistTail) :-
    apply_sudo(CmdArgs, Ctx0, Command, Ctx),
    editing_command(Command), !,
    trace_editing_command(Command, Ctx, WrittenFiles),
    debug(executing, "wrote to ~w", [WrittenFiles]),
    HistTail = [edited(WrittenFiles)-Ctx|HistTail1],
    loop(In, Ctx0, Hist-HistTail1).
% execute by default
handle_single(Command-HistCommand, Redirects, Ctx, In, H-Ht) :-
    run_command(Command, Redirects, Ctx, Status),
    debug(executing, "Status ~w", [Status]),
    ( Status == exit(0)
    -> Ht = [HistCommand-Ctx.put([redirects=Redirects])|Htt]
    ;  Ht = Htt ),
    loop(In, Ctx, H-Htt).
% if couldn't execute, print error & continue
handle_single(Command-_, _, Env, In, H) :-
    format(user_error, "error executing command ~w~n", [Command]),
    loop(In, Env, H).

% Running commands

%! editing_command(+Command:list) is semidet.
%
%  True when Command is running an editor.
editing_command(["vi"|_]).
editing_command(["nano"|_]).

%! redirects_in_out_err(+Redirects:list, -In, -Out, -Err) is det.
%
%  True when =Redirects= is a list of redirect terms (see
%  =parsing:redirect//1=) and =In=, =Out=, and =Err= are terms in the
%  form =Stream=-=Close=, where =Stream= is the corresponding stream
%  and =Close= is a closure to close the stream, if needed.
redirects_in_out_err(Redirects, InStream-CloseIn, OutStream-CloseOut, ErrStream-CloseErr) :-
    set_outputs(Redirects, _{}, StreamsConf),
    ( get_dict(in, StreamsConf, _{stream: InStream, close: CloseIn})
    -> true
    ; ( InStream = std, CloseIn = true ) ),
    ( get_dict(out, StreamsConf, _{stream: OutStream, close: CloseOut})
    -> true
    ; ( CloseOut = true,
        ( ( stream_property(current_output, file_no(OutFileNo)),
            OutFileNo \= 1 )
        ->  OutStream = stream(current_output)
        ;   OutStream = std )
      )
    ),
    ( get_dict(err, StreamsConf, _{stream: ErrStream, close: CloseErr})
    -> true
    ; ( ErrStream = std, CloseErr = true ) ).

%! run_command(+Command:list, +Redirects:list, +Context:dict, -Status) is det.
%
%  Executes the command specified by =CommandArgs=, redirecting
%  in/out/err as specified by =Redirects=, with the context =Context=
%  and returns the status =Status=.
run_command(CommandArgs, Redirects, Ctx, Status) :-
    run_command(CommandArgs, Redirects, Ctx, Status, true).

%! run_command(+Command:list, +Redirects:list, +Context:dict, -Status, +Wait:bool) is det.
%
%  Like run_command/4, except the =Wait= argument, which determines if
%  it should wait for the process to finish. If false, meaning don't
%  wait, then =Status= will be the pid of the spawn process instead of
%  the return code.
run_command([Command|Args], Redirects, Ctx, Status, Wait) :-
    env_list(Ctx.env, EnvList),
    env_get("PATH", Ctx.env, PathStr),
    split_string(PathStr, ":", "", Paths),
    member(Dir, Paths),
    absolute_file_name(Command, AbsCommand,
                       [ relative_to(Dir),
                         access(execute),
                         file_errors(fail) ]),
    debug(executing, "~w -> ~w", [Command, AbsCommand]),
    % [TODO] Also return the actual AbsCommand?
    setup_call_cleanup(
        ( prompt(OldPrompt, ''),
          redirects_in_out_err(Redirects, InStream-CloseIn, OutStream-CloseOut, ErrStream-CloseErr),
          process_set_method(vfork),
          process_create(AbsCommand, Args,
                         [ environment(EnvList),
                           cwd(Ctx.cwd),
                           stdin(InStream),
                           stdout(OutStream),
                           stderr(ErrStream),
                           process(Pid) ])
        ),
        ( debug(executing, "Pid ~w", [Pid]),
          ( Wait -> process_wait(Pid, Status) ; Status = Pid ),
          debug(executing, "Finished process", [])
        ),
        ( CloseIn, CloseOut, CloseErr,
          prompt(_, OldPrompt)
        )
    ),
    !.

run_pipeline(Commands, Ctx, Status) :-
    run_pipeline(Commands, std, Ctx, Pids-Pids, Status0),
    maplist([Pid, Status]>>process_wait(Pid, Status),
            Pids,
            Statuses),
    ( ( member(exit(Ex), Statuses),
        Ex \= 0 )
    -> Status = exit(Ex)
    ;  Status = Status0 ).

run_pipeline([], _, _, _).
run_pipeline([command(Cmd, Redirects)], InRedirects, Ctx, _-[], Status) :-
    !,
    run_command(Cmd, [from(pipe(InRedirects))|Redirects], Ctx, Status, true).
run_pipeline([command(Cmd, Redirects0)|Commands], InRedirects, Ctx, Pids-Tail0, Status) :-
    append(Redirects0, [to(pipe(PipeStream))], Redirects),
    run_command(Cmd, [from(pipe(InRedirects))|Redirects], Ctx, Pid, false),
    Tail0 = [Pid|Tail1],
    run_pipeline(Commands, PipeStream, Ctx, Pids-Tail1, Status).

% Helper predicates

filedescriptor_name(u(`0`), in).
filedescriptor_name(u(`1`), out).
filedescriptor_name(u(`2`), err).

expand_redirect_name(_Ctx, F, S) :-
    F =.. [T, fd(Str)],
    filedescriptor_name(Str, Name),
    !, S =.. [T, fd(Name)].
expand_redirect_name(Ctx, F, S) :-
    F =.. [T, Location],
    sh_string_val(Ctx.env, Location, ExLoc0),
    directory_file_path(Ctx.cwd, ExLoc0, ExLoc),
    !, S =.. [T, ExLoc].
expand_redirect_name(_, F, F).

set_outputs([], Outs, Outs).
set_outputs([Redirect|Redirects], Outs0, Outs) :-
    set_output(Redirect, Outs0, Outs1),
    set_outputs(Redirects, Outs1, Outs).

set_output(from(pipe(std)), Out, Out) :- !.
set_output(from(pipe(PipeStr)), Out0, Out1) :- !,
    Out1 = Out0.put(in, _{stream: stream(PipeStr),
                           close: true}).
set_output(from(S), Out0, Out1) :-
    open(S, read, In, [bom(false)]),
    Out1 = Out0.put(in, _{stream: stream(In),
                          close: close(In)}).
set_output(to(fd(2)), Out0, Out1) :-
    get_dict(err, Out0, ErrConf), !,
    Out1 = Out0.put(out, _{stream: stream(ErrConf.stream),
                           close: true}).
set_output(to(fd(2)), Out0, Out1) :- !,
    stream_property(UserErr, alias(user_error)),
    Out1 = Out0.put(out, _{stream: stream(UserErr),
                           close: true}).
set_output(to(pipe(PipeStr)), Out0, Out1) :- !,
    Out1 = Out0.put(out, _{stream: pipe(PipeStr),
                           close: true}).
set_output(to(F), Out0, Out1) :-
    open(F, write, Out, []),
    Out1 = Out0.put(out, _{stream: stream(Out),
                           close: close(Out)}).
set_output(append(F), Out0, Out1) :-
    open(F, append, Out, []),
    Out1 = Out0.put(out, _{stream: stream(Out),
                           close: close(Out)}).
set_output(error(fd(1)), Out0, Out1) :-
    get_dict(out, Out0, OutConf), !,
    Out1 = Out0.put(err, _{stream: OutConf.stream,
                           close: true}).
set_output(error(fd(1)), Out0, Out1) :- !,
    stream_property(UserOut, alias(current_output)),
    Out1 = Out0.put(err, _{stream: stream(UserOut),
                           close: true}).
set_output(error(F), Out0, Out1) :-
    open(F, write, Err, []),
    Out1 = Out0.put(err, _{stream: stream(Err),
                           close: close(Err)}).
set_output(both(F), Out0, Out2) :-
    open(F, write, Both, []),
    Out1 = Out0.put(out, _{stream: stream(Both),
                           close: close(Both)}),
    Out2 = Out1.put(err, _{stream: stream(Both),
                           close: true}).

%! apply_sudo(+Command0:list, +Ctx0:dict, -Command1:list, -Ctx1:dict) is det.
%
%  Get the base command, stripping off the "sudo [-u $user]" prefix, if any.
apply_sudo(MaybeSudoCommand, Ctx0, BaseCommand, Ctx1) :-
    sudo_command(MaybeSudoCommand, Ctx0, BaseCommand, Ctx1), !.
apply_sudo(Command, Ctx, Command, Ctx).

%! difflist_list(+DiffListH, -DiffListT, -List) is det.
%
%  True when =DiffListH= & =DiffListT= form a difference list, =List=
%  is a proper list representing the current instantiation of the
%  difference list
difflist_list(Codes, Rst, []) :-
    var(Codes), Codes == Rst, !.
difflist_list([C|Cs], Rst, [C|More]) :- !,
    difflist_list(Cs, Rst, More).
difflist_list([], _, []).
