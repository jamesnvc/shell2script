:- module(tracing, [trace_editing_command/3]).
/** <module> Run editing commands under strace

Module for running editing commands under =strace(1)= to be able to tell
what files are being edited.

@author James Cash
*/

:- use_module(library(apply), [exclude/3]).
:- use_module(library(assoc), [empty_assoc/1,
                               get_assoc/3,
                               put_assoc/4,
                               del_assoc/4]).
:- use_module(library(dcg/basics), [digits//1,
                                    integer//1,
                                    white//0,
                                    string_without//2,
                                    whites//0,
                                    blanks//0]).
:- use_module(library(debug), [debug/3]).
:- use_module(library(dcg/high_order), [optional//2]).
:- use_module(library(edcg)).
:- use_module(library(filesex), [copy_file/2, directory_file_path/3]).
:- use_module(library(lists), [member/2]).
:- use_module(library(rdet)).
:- use_module(library(process), [process_create/3, process_wait/2]).
:- use_module(library(pure_input), [phrase_from_file/2, phrase_from_stream/2]).
:- use_module(library(syslog), [closelog/0, openlog/3, syslog/3]).
:- use_module(library(solution_sequences), [distinct/2]).
:- if((current_prolog_flag(unix, true), \+ current_prolog_flag(apple, true))).
:- use_module(library(inotify), [inotify_init/2,
                                 inotify_close/1,
                                 inotify_add_watch/3,
                                 inotify_read_event/3]).
:- else.
inotify_init(_, _).
inotify_close(_).
inotify_add_watch(_, _, _).
inotify_read_event(_, _, _).
:- endif.
:- use_module(commands, [env_list/2,
                         env_get/3]).

:- dynamic edcg:pred_info/3.
:- dynamic edcg:acc_info/5.

user_name(UserName) :-
    getuid(Uid),
    user_info(Uid, UserData),
    user_data(name, UserData, UserName).

strace_command(Ctx, "/usr/bin/sudo",
               ["/usr/bin/strace", "-u", User|Tail]-Tail) :-
    get_dict(as, Ctx, User), !.
strace_command(_, "/usr/bin/strace", L-L).

touch(File) :-
    setup_call_cleanup(
        open(File, update, S, []),
        true,
        close(S)
    ).

%! trace_editing_command(+EditCommand:list, +Ctx:dict, -Files:list) is det.
%
%  Run the editing command & arguments in EditCommand, with the
%  context (environment & optionally as(User)) Ctx under strace,
%  to keep track of which files are written to in the editing session.
%  Files will be a list of path names for the written-to files.
trace_editing_command(EditingCommandArgs, Ctx, WrittenFiles-Diffs) :-
    env_list(Ctx.env, EnvList),
    tmp_file(strace_out, TmpFile),
    strace_command(Ctx, Command, Args-Tail),
    Tail = ["-o", TmpFile,
            "-f",
            "-e", "trace=open,openat,close,write,chdir,fchdir"
            |EditingCommandArgs],
    empty_assoc(EmptyFdFiles),
    empty_assoc(EmptyFileCopy),
    empty_assoc(FileDiff),
    openlog(bard, [], syslog),
    touch(TmpFile), % file needs to exist for monitoring thread
    setup_call_cleanup(
        ( prompt(OldPrompt, ''),
          process_create(Command, Args,
                         [ environment(EnvList),
                           cwd(Ctx.cwd),
                           process(Pid) ]),
          message_queue_create(ParseQ, []),
          thread_create(
              monitor_strace_output(ParseQ,
                                    TmpFile,
                                    _{cwd: Ctx.cwd,
                                      file_diff: FileDiff,
                                      file_copy: EmptyFileCopy,
                                      fd_file: EmptyFdFiles}),
              ThreadId,
              [])
        ),
        process_wait(Pid, _Status),
        ( prompt('', OldPrompt),
          syslog(debug, "done; end monitor thread ~w", [ThreadId]),
          thread_send_message(ThreadId, done),
          thread_get_message(ParseQ, Diffs),
          message_queue_destroy(ParseQ),
          thread_join(ThreadId)
        )
    ),
    closelog,
    debug(tracing, "diffs ~w", [Diffs]),
    extract_strace_output(Ctx.cwd, TmpFile, Traces),
    %% debug(tracing, "traces ~w", [Traces]),
    written_to(Traces, WrittenFiles0),
    exclude(swap_file, WrittenFiles0, WrittenFiles).

monitor_strace_output(ParseQ, TmpFile, State) :-
    inotify_init(INotify, []),
    inotify_add_watch(INotify, TmpFile, [modify]),
    catch(monitor_strace_output_loop(ParseQ, INotify, 0, State, L-L),
          Ex,
          syslog(debug, "Exception in monitor loop: ~w", [Ex])
         ),
    syslog(debug, "Finished strace monitor", []).
monitor_strace_output_loop(ParseQ, INotify, Offset, State0, List-Tail0) :-
    inotify_read_event(INotify, modify(file(File)), [timeout(0.5)]),
    open(File, read, InStream, []),
    seek(InStream, Offset, bof, _),
    phrase_from_stream(strace_output(Lines), InStream),
    handle_strace_monitor(Lines, State0, State1, List-Tail0, Tail),
    stream_property(InStream, position(StreamPos)),
    stream_position_data(byte_count, StreamPos, NewOffset),
    !,
    ( check_done(ParseQ, State1)
    ; monitor_strace_output_loop(ParseQ, INotify, NewOffset, State1, List-Tail) ).
monitor_strace_output_loop(ParseQ, INotify, Offset, State, List-Tail) :-
    check_done(ParseQ, State)
    ; monitor_strace_output_loop(ParseQ, INotify, Offset, State, List-Tail).

check_done(ParseQ, State) :-
    thread_peek_message(done),
    thread_send_message(ParseQ, State.file_diff).

%% :- rdet(handle_strace_monitor/5).

handle_strace_monitor(Lines-[], State0, State, _List-Lines1, Tail) :-
    phrase(handle_traces(Lines1-Tail,
                         State0.fd_file, Assoc,
                         State0.cwd, Cwd),
           Lines, []), !,
    put_dict(_{cwd: Cwd, fd_file: Assoc}, State0, State1),
    maybe_store_files(Lines1, State1, State).

maybe_store_files(Tail, State, State) :- var(Tail), !.
maybe_store_files([Trace|Traces], State0, State) :-
    once(maybe_store_file(Trace, State0, State1)),
    maybe_store_files(Traces, State1, State).

maybe_store_file(open(F, _, _), S, S) :-
    % if it's a swap file, don't care
    swap_file(F), !.
maybe_store_file(open(_, _, Opts), S, S) :-
    memberchk(flags(Flags), Opts),
    memberchk(flag(close_on_exec), Flags),
    !. % if a file was opened O_CLOEXEC, it's probably a library
maybe_store_file(open(F, _, Opts), State0, State) :-
    memberchk(flags(Flags), Opts),
    writable_flags(Flags),
    exists_file(F), !,
    % [XXX] looks like vi doesn't actually open it writably until you
    % try to save...
    save_for_diffing(F, State0, State).
maybe_store_file(open(F, _, _), State0, State) :-
    % [TODO] check size?
    % [TODO] check return code? -1 is an error, probably doesn't exist
    save_for_diffing(F, State0, State).
maybe_store_file(write(Filename), State0, State1) :-
    get_assoc(Filename, State0.file_copy, OrigFile),
    compute_diff(OrigFile, Filename, Diff),
    put_assoc(Filename, State0.file_diff, Diff, FileDiffs),
    put_dict(_{file_diff: FileDiffs}, State0, State1).
maybe_store_file(close(File), State0, State) :-
    dispose_copy(File, State0, State).
maybe_store_file(_, S, S).

save_for_diffing(F, State, State) :-
    get_assoc(F, State.file_copy, _), !.
save_for_diffing(F, State0, State) :-
    exists_file(F), !,
    tmp_file(diff_copy, TmpCopy),
    % syslog(debug, "saving ~w to ~w for later diffing", [F, TmpCopy]),
    copy_file(F, TmpCopy),
    put_assoc(F, State0.file_copy, TmpCopy, NewFileCopy),
    put_dict(_{file_copy: NewFileCopy}, State0, State).
save_for_diffing(F, State0, State) :-
    % file doesn't exist, so "previous copy" is /dev/null
    put_assoc(F, State0.file_copy, '/dev/null', NewFileCopy),
    put_dict(_{file_copy: NewFileCopy}, State0, State).

compute_diff(File1, File2, Diff) :-
    setup_call_cleanup(
        process_create("/usr/bin/diff",
                       ["--new-file", % treat absent files as empty
                        "--unified",
                        File1, File2],
                       [stdout(pipe(Str)),
                        process(Pid)]),
        read_string(Str, _, Diff),
        process_wait(Pid, _Status)
    ).

dispose_copy(_, State, State) :- !. % tmp, don't delete on close
dispose_copy(F, State0, State) :-
    del_assoc(F, State0.file_copy, TmpFile, NewFileCopy),
    % syslog(debug, "removing copy of ~w at ~w", [F, TmpFile]),
    catch(delete_file(TmpFile),
          error(existence_error(file, TmpFile), _Ctx),
          syslog(debug, "copy of ~w @ ~w doesn't exist", [F, TmpFile])),
    put_dict(_{file_copy: NewFileCopy}, State0, State).

swap_file(Filename) :-
    string_concat(_, ".swp", Filename).
swap_file(Filename) :-
    string_concat(_, ".swo", Filename).
swap_file(Filename) :-
    string_concat(_, "~", Filename).

% Parsing strace output

quoted_string(S) -->
    "\"", quoted_string_aux(Codes),
    { string_codes(S, Codes) }.
quoted_string_aux([]) --> "\"".
quoted_string_aux([0'"|Cs]) -->
    "\\", "\"", !,
    quoted_string_aux(Cs).
quoted_string_aux([C|Cs]) -->
    [C],
    quoted_string_aux(Cs).

prefix -->
    digits(_Pid), whites.

open_flag(read_only) --> "O_RDONLY".
open_flag(write_only) --> "O_WRONLY".
open_flag(read_write) --> "O_RDWR".
open_flag(non_blocking) --> "O_NONBLOCK".
open_flag(append) --> "O_APPEND".
open_flag(create) --> "O_CREAT".
open_flag(truncate) --> "O_TRUNC".
open_flag(non_existing) --> "O_EXCL".
open_flag(shared_lock) --> "O_SHLOCK".
open_flag(exclusive_lock) --> "O_EXLOCK".
open_flag(no_follow) --> "O_NOFOLLOW".
open_flag(symlinks) --> "O_SYMLINK".
open_flag(event_only) --> "O_EVTONLY".
open_flag(close_on_exec) --> "O_CLOEXEC".
open_flag(directory) --> "O_DIRECTORY".

writable_flags(Flags) :- memberchk(write_only, Flags), !.
writable_flags(Flags) :- memberchk(read_write, Flags).

flags([Flag|Flags]) --> open_flag(Flag), !, flags_(Flags).
flags_([Flag|Flags]) --> "|", open_flag(Flag), !, flags_(Flags).
flags_([]) --> [].

mode(Mode) --> digits(Mode).

rest_of_line --> string_without("\n", _), "\n".

strace_line(open(File, Fd, [flags(Flags)|MaybeMode])) -->
    prefix,
    "openat(AT_FDCWD,", !, whites,
    quoted_string(File),
    ",", whites,
    flags(Flags),
    optional(
        ( ",", whites,
          mode(Mode),
          { MaybeMode = [mode(Mode)] } ),
        { MaybeMode = [] }),
    !,
    whites,
    ")", whites, "=", whites,
    integer(Fd),
    rest_of_line.
strace_line(open_rel(DirFd, File, Fd, [flags(Flags)|MaybeMode])) -->
    prefix,
    "openat(", integer(DirFd), !,
    ",", whites,
    quoted_string(File),
    ",", whites,
    flags(Flags),
    optional((",", whites, mode(Mode),
              { MaybeMode = [mode(Mode)] }),
             { MaybeMode = [] }),
    !,
    whites,
    ")", whites, "=", whites,
    integer(Fd),
    rest_of_line.
strace_line(open(File, Fd, [flags(Flags)|MaybeMode])) -->
    prefix,
    "open(", !,
    quoted_string(File), ",", % file being opened
    whites,
    flags(Flags),
    optional((",", whites, mode(Mode),
              { MaybeMode = [mode(Mode)] }),
             { MaybeMode = [] }),
    !,
    whites,
    ")", whites, "=", whites,
    integer(Fd),
    rest_of_line.
strace_line(write(Fd)) -->
    prefix,
    "write(", !, integer(Fd), ",",
    rest_of_line.
strace_line(chdir(Dir, Ret)) -->
    prefix,
    "chdir(", quoted_string(Dir), ")", !,
    whites, "=", whites,
    integer(Ret),
    rest_of_line.
strace_line(chdir_fd(DirFd, Ret)) -->
    prefix,
    "chdir(", integer(DirFd), ")", !,
    whites, "=", whites,
    integer(Ret),
    rest_of_line.
strace_line(chdir_fd(DirFd, Ret)) -->
    prefix,
    "fchdir(", integer(DirFd), ")", !,
    whites, "=", whites,
    integer(Ret),
    rest_of_line.
strace_line(close(Fd)) -->
    prefix,
    "close(", integer(Fd), ")",
    whites, "=", whites,
    integer(_Ret), rest_of_line.
strace_line(other(Stuff)) -->
    string_without("\n", Stuff), "\n".


strace_output([Line|Lines]-Tail) -->
    strace_line(Line), !,
    strace_output(Lines-Tail).
strace_output(Tail-Tail) --> [].

% Processing parsed output

%! expand_path(+Path0:atom, -Path1:atom) is det.
%
%  Very simplified version of absolute_file_name/2 to just expand dot
%  and double-dot path names. Using this instead of
%  absolute_file_name/2 because that does file IO to check that the
%  file exists which both isn't what we necessarily want here and is
%  quite slow
expand_path(Path0, Path1) :-
    atomic_list_concat(Parts0, '/', Path0),
    exclude(==('.'), Parts0, Parts1),
    expand_path_twodot(Parts1, Parts2),
    atomic_list_concat(Parts2, '/', Path1).
expand_path_twodot([_, '..'|Rest0], [Rest1]) :-
    expand_path_twodot(Rest0, Rest1).
expand_path_twodot([P|Rest0], [P|Rest1]) :-
    expand_path_twodot(Rest0, Rest1).
expand_path_twodot([], []).

% helper predicates for edcg
assoc_add_remove(-K, A0, A1) :-
    del_assoc(K, A0, _, A1).
assoc_add_remove(+(K, V), A0, A1) :-
    put_assoc(K, A0, V, A1).

edcg:acc_info(assoc, OpKV, A0, A1, assoc_add_remove(OpKV, A0, A1)).
edcg:acc_info(cwd, Ret-NewCwd, _, NewCwd, Ret == 0).

edcg:pred_info(handle_trace, 1, [assoc, cwd, dcg]).
edcg:pred_info(handle_traces, 1, [assoc, cwd, dcg]).

handle_trace(open(AbsFile, Fd, Opts)) -->>
    [open(RelFile, Fd, Opts)]:dcg,
    /(Cwd, cwd),
    { directory_file_path(Cwd, RelFile, File),
      % absolute_file_name/2 is really slow here, presumably because
      % it's checking that the files actually exist
      expand_path(File, AbsFile) },
    [+(Fd, AbsFile)]:assoc.
handle_trace(open(AbsFile, Fd, Opts)) -->>
    [open_rel(DirFd, RelFile, Fd, Opts)]:dcg,
    A/assoc,
    { get_assoc(DirFd, A, Dir),
      directory_file_path(Dir, RelFile, File),
      expand_path(File, AbsFile) },
    [+(Fd, AbsFile)]:assoc.
handle_trace(close(File)) -->>
    [close(Fd)]:dcg,
    A/assoc,
    { get_assoc(Fd, A, File) },
    [-Fd]:assoc.
handle_trace(write(File)) -->>
    [write(Fd)]:dcg,
    A/assoc,
    { get_assoc(Fd, A, File) }.
handle_trace(chdir(Dir, Ret)) -->>
    [chdir_fd(Fd, Ret)]:dcg,
    A/assoc,
    { get_assoc(Fd, A, Dir) },
    [Ret-Dir]:cwd.
handle_trace(chdir(Dir, Ret)) -->>
    [chdir(Dir, Ret)]:dcg,
    [Ret-Dir]:cwd.
handle_trace(other(X)) -->> [X]:dcg.

handle_traces([O|Os]-Tail) -->>
    handle_trace(O), !, handle_traces(Os-Tail).
handle_traces(Tail-Tail) -->> [].

%! canonicalize_path_names(+Cwd:path, +Traces0:list, -Traces1:list) is det.
%
%  Process the strace data to make path names absolute, handling
%  changes to the cwd inside the process.
canonicalize_path_names(Cwd, Traces0, Traces1) :-
    empty_assoc(A0),
    phrase(handle_traces(Traces1-[], A0, _, Cwd, _), Traces0).

extract_strace_output(Cwd, File, Traces) :-
    phrase_from_file(strace_output(Traces0-[]), File),
    canonicalize_path_names(Cwd, Traces0, Traces).

%! written_to(+StraceLines:list, -Files:list) is det.
%
%  True when StraceLines is a list of parsed lines from strace's
%  output and Files is a list of the names of the files opened &
%  written to
written_to(Traces, Files) :-
    findall(F, distinct(F, member(write(F), Traces)), Files).
