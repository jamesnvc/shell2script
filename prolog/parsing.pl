:- module(parsing, [line//2]).
/** <module> DCGs for tokenizing shell lines

This module contains DCGs for parsing the typed-in lines in the shell
into something more structured, to be parsed further by the =command=
module.

@author James Cash
@see commands.pl
*/
:- use_module(library(dcg/basics), [string_without//2,
                                    white//0,
                                    whites//0]).

word(u([var(V)])) --> var(V).
word(s(W)) --> "'", !, single_quoted(W).
word(d(W)) -->
    "\"", !, double_quoted(W).
word(u(W)) -->
    unquoted_word(W).

var(V) --> "${", !, var_name(V), "}".
var(V) --> "$", var_name(V).

var_name([V|Vs]) -->
    [V],
    { code_type(V, csym) },
    !, var_name_(Vs).
var_name_(Vs) --> var_name(Vs), !.
var_name_([]) --> [].

single_quoted([]) --> "'", !.
single_quoted(Cs) -->
    "\\", "\n", !, single_quoted(Cs).
single_quoted([C|Cs]) -->
    "\\", [C], !, single_quoted(Cs).
single_quoted([C|Cs]) -->
    [C], single_quoted(Cs).

double_quoted([]) --> "\"", !.
double_quoted(Cs) -->
    "\\", "\n", !, double_quoted(Cs).
double_quoted([0'\n|Cs]) -->
    "\\", "n", !, double_quoted(Cs).
double_quoted([C|Cs]) -->
    "\\", [C], !, double_quoted(Cs).
double_quoted([var(Var)|Cs]) -->
    var(Var), !, double_quoted(Cs).
double_quoted([C|Cs]) -->
    [C], double_quoted(Cs).

% check so that an error redirect like `{command} 2> foo` doesn't get
% parsed as Words = `{command} 2`, Redirects = `> foo`.
check_not_redirect(0'2), [Peek] -->
    [Peek], !,
    { Peek \= 0'> }.
check_not_redirect(_) --> [].

unquoted_word([C|Cs]) -->
    "\\", [C],
    { code_type(C, space) },
    !, unquoted_word_(Cs).
unquoted_word([var(Var)|Cs]) -->
    var(Var), !, unquoted_word_(Cs).
unquoted_word([C|Cs]) -->
    [C], check_not_redirect(C),
    { \+ memberchk(C, `\n ;&|><`) },
    !, unquoted_word_(Cs).
% structuring in this kind of odd way to ensure that the "unquoted
% word" isn't an empty string
unquoted_word_([]) --> white, !.
unquoted_word_(Cs) --> unquoted_word(Cs), !.
unquoted_word_([]) --> [].

var_set(var_set(V, Val)) -->
    var_name(V), "=", word(Val).

var_sets([V|Vs]) --> var_set(V), !, whites, var_sets(Vs).
var_sets([]) --> [].

words([W|Ws]) --> word(W), !, whites, words(Ws).
words([]) --> [].

redirect(from(Src)) --> "<", whites, word(Src).
redirect(error(fd(Fd))) --> "2>&", whites, word(Fd).
redirect(error(Dst)) --> "2>", whites, word(Dst).
redirect(to(fd(Fd))) --> ">&", whites, word(Fd).
redirect(append(Dst)) --> ">>", whites, word(Dst).
redirect(to(Dst)) --> ">", whites, word(Dst).
redirect(both(Dst)) --> "&>", whites, word(Dst).

redirects([R|Rs]) --> redirect(R), !, whites, redirects(Rs).
redirects([]) --> [].

single_command(command(Words, Redirects)) -->
    words(Words), whites, redirects(Redirects).

command(pipe(C1, C2)) -->
    single_command(C1), whites, "|", !, whites,
    command(C2).
command(C) -->
    single_command(C).

%! line(-Variables:list, -Command:term)// is semidet.
%
%  Parse a newline-terminated line into variable settings, words, and
%  redirects.
line(Vars, Command) -->
    var_sets(Vars), whites,
    command(Command), whites, "\n".
