:- module(line_bindings, [setup_shell_editline/0,
                          shell_history_add/1]).
/** <module> Bindings for libeditline

Configure libeditline for a reasonable shell experience

@author James Cash
*/
:- use_module(library(debug), [debug/3]).
:- use_module(library(editline), [el_add_history/2,
                                  el_wrap/4,
                                  el_unwrap/1,
                                  el_bind/2,
                                  el_source/2,
                                  el_addfn/4,
                                  el_line/2,
                                  el_insertstr/2,
                                  el_deletestr/2,
                                  el_cursor/2,
                                  el_wrapped/1]).

setup_shell_editline :-
    el_wrapped(user_input),
    debug(executing, "Already wrapped", []),
    el_unwrap(user_input),
    fail.
setup_shell_editline :-
    stream_property(user_input, tty(true)), !,
    debug(executing, "Setting up editline", []),
    % [TODO] add tab completion
    el_wrap(bard, user_input, user_output, user_error).
setup_shell_editline.

shell_history_add(Line) :-
    el_add_history(user_input, Line).
