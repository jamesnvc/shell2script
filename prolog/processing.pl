:- module(processing, [process_history/2,
                       sudo_command/4]).
/** <module> Processing command history

Module for processing the command history from the =executing= module,
to be later used in =output=.

@author James Cash
*/

:- use_module(library(lists), [member/2]).
:- use_module(utils, [any/2]).

%! process_history(+Commands, -Changes) is det.
%
%  Given a list of pairs of the raw commands run & their context in
%  Commands, Changes will be a list of the simplified "changes" that
%  those commands affected
process_history([], []).
process_history([C|Cs], [S|Ss]) :-
    mutating_command(C, S), !,
    process_history(Cs, Ss).
process_history([_|Cs], Ss) :-
    % \+ mutating_command(C, S),
    process_history(Cs, Ss).

%! sudo_command(+Command0:list, +Ctx0:dict, -Command1:list, -Ctx1:dict) is semidet.
%
%  Determine if Command0 is running a command as another user via `sudo`
%  and unifies Command1 with the base command and Ctx1 with Ctx0 with
%  the `as` key indicating which user the command is being run as.
%  False if the command is not using sudo.
sudo_command(["sudo", "-u", User|Command], Ctx0, Command, Ctx1) :-
    !, put_dict(as, Ctx0, User, Ctx1).
sudo_command(["sudo"|Command], Ctx0, Command, Ctx1) :-
    put_dict(as, Ctx0, "root", Ctx1).


%! mutating_command(+ArgsContext, -Command:term) is det.
%
%  True when =Command= is a compound term representing what the
%  command given by =Args= performed, with the given =Context=
%  [TODO] break this up into is_mutating & process_command?
mutating_command(edited(Files)-Ctx, edited(Files, Ctx)).
mutating_command(["rm"|Args]-Ctx, remove(Args, Ctx)).
mutating_command(["touch"|Args]-Ctx, create(Args, Ctx)).
mutating_command(["sed"|Args]-Ctx, stream_edit(Args, Ctx)).
mutating_command(["ufw"|Args]-Ctx, firewall(Args, Ctx)).
mutating_command(["apt", "install"|Packages]-Ctx, package_install(Packages, Ctx)).
mutating_command(["apt-get", "install"|Packages]-Ctx, package_install(Packages, Ctx)).
mutating_command(["systemctl"|Args]-_Ctx, system_control(Args)).
mutating_command(pipeline(Commands, Ctx), pipeline(Commands, Ctx)) :-
    member(command(Cmd, Redirs), Commands),
    put_dict(redirects, Ctx, Redirs, Ctx1),
    mutating_command(Cmd-Ctx1, _), !.
mutating_command(SudoCommand-Ctx0, Change) :-
    sudo_command(SudoCommand, Ctx0, Command, Ctx1),
    mutating_command(Command-Ctx1, Change).
mutating_command(Cmd-Ctx, output(Cmd, Ctx)) :-
    get_dict(redirects, Ctx, Redirects),
    any(member(T, [to, append, error]),
        ( F =.. [T, Dest],
          memberchk(F, Redirects),
          Dest \= fd(_) )
       ).
