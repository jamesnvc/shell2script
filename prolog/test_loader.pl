:- module(test_loader, []).
/** <module> Test Loader -- dummy module to load all tests from sibling directory

@author James Cash
*/

:- use_module(library(apply), [include/3, maplist/3]).
:- use_module(library(filesex), [directory_file_path/3,
                                 relative_file_name/3]).
:- use_module(library(plunit), [run_tests/0]).
:- use_module(library(yall)).

:- initialization(load_tests).

load_tests :-
    module_property(test_loader, file(ThisFile)),
    relative_file_name(TestDir, ThisFile, '../test'),
    directory_files(TestDir, Files),
    include([F]>>file_name_extension(_, plt, F),
            Files, TestFileNames),
    maplist({TestDir}/[N, F]>>directory_file_path(TestDir, N, F),
            TestFileNames, TestFiles),
    load_files(TestFiles),
    run_tests.
