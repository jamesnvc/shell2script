:- module(utils, [any/2]).

:- meta_predicate any(:, ^).

%! any(:Generator, ^Body) is semidet.
%
%  True if, for any bindings of =Generator=, =Body= is true.
any(Generator, Body) :-
    Generator,
    ( Body
    -> !
    ;  fail ).
