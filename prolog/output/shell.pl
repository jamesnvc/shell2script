:- module(shell, [write_shell_script/2,
                  redirects_suffix/2]).
/** <module> Shell script output

Output command history as a bash script.

@author James Cash
*/

:- use_module(library(apply), [convlist/3, exclude/3]).
:- use_module(library(apply_macros)).
:- use_module(library(assoc), [assoc_to_list/2]).
:- use_module(library(debug), [debug/3]).
:- use_module(library(lists), [member/2]).
:- use_module(library(uuid), [uuid/1]).
:- use_module(library(yall)).

:- discontiguous term_expansion/2.

%! write_shell_script(+Stream:stream, +Changes:list) is det.
%
%  Write a shell script to stream =Stream= that will reproduce the
%  changes specified in =Changes=.
write_shell_script(Stream, Changes) :-
    format(Stream, "#!/usr/bin/env bash~n~nset -euo pipefail~n~n", []),
    forall(
        member(Change, Changes),
        % [TODO] be smarter about setting directory
        % only do it for commands that need it?
        % at least, only do it when different
        ( Change =.. [_, _, Ctx],
          format(Stream, "cd ~w~n", [Ctx.cwd]),
          output_change(Stream, Change) )
    ).

sudo_prefix(Ctx, "sudo ") :-
    _{as: "root"} :< Ctx, !.
sudo_prefix(Ctx, SudoU) :-
    _{as: User} :< Ctx, !,
    format(string(SudoU), "sudo -u ~w ", [User]).
sudo_prefix(_, "").

term_expansion(redirect(Func, Char),
               (redirect_command(Term, Command) :-
                    format(string(Command), "~w ~w", [Char, F]))) :-
    Term =.. [Func, F].

redirect_command(error(fd(Fd)), Command) :-
    format(string(Command), "2>&~w", [Fd]).
redirect_command(to(fd(Fd)), Command) :-
    format(string(Command), ">&~w", [Fd]).
redirect(from, "<").
redirect(to, ">").
redirect(append, ">>").
redirect(error, "2>").

redirects_suffix(Redirects, Suffix) :-
    convlist(redirect_command, Redirects, Suffixes),
    atomic_list_concat([''|Suffixes], " ", Suffix).

is_blank(Atomic) :- normalize_space(atom(''), Atomic).

output_basic_command(Stream, Ctx, Command, Args) :-
    sudo_prefix(Ctx, Pre),
    redirects_suffix(Ctx.redirects, RedirectSuffix),
    exclude(is_blank, [Command|Args], FilteredArgs),
    atomic_list_concat(FilteredArgs, ' ', SArgs),
    format(Stream, "~w~w~w~n", [Pre, SArgs, RedirectSuffix]).

term_expansion(basic_command(Term, Command),
               (output_change(Stream, Func) :-
                 output_basic_command(Stream, Ctx, Command, Args))) :-
    Func =.. [Term, Args, Ctx].

% [TODO] Handle quoting args properly
% [TODO] handle flags to commands
basic_command(create, "touch").
basic_command(remove, "rm").
basic_command(output, "").
basic_command(stream_edit, "sed").
basic_command(firewall, "ufw").
basic_command(package_install, "apt install").
output_change(Stream, edited(_Files-DiffsAssoc, Ctx)) :-
    sudo_prefix(Ctx, Pre),
    assoc_to_list(DiffsAssoc, Diffs),
    forall(
        member(_File-Diff, Diffs),
        ( uuid(Marker),
          format(Stream, "~wpatch --directory=/ --strip=0 <<~w~n~w~w~n",
                 [Pre, Marker, Diff, Marker]) )
    ).
output_change(Stream, pipeline(Commands, Ctx)) :-
    maplist({Ctx}/[command(Cmd, Redirs), Str]>>(
                sudo_prefix(Ctx, Pre),
                redirects_suffix(Redirs, RedirSuffix),
                atomic_list_concat(Cmd, ' ', SCmd),
                format(string(Str), "~w~w~w", [Pre, SCmd, RedirSuffix])
            ), Commands, CommandStrs),
    atomic_list_concat(CommandStrs, ' | ', Pipeline),
    format(Stream, "~w~n", [Pipeline]).
output_change(Stream, Change) :-
    format(Stream, "# ~k~n", [Change]).
