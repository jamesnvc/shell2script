:- module(prolog_terms, [write_prolog_script/2]).
/** <module> Output as Prolog Terms

Simple output backend of just plain Prolog terms.

@author James Cash
*/

%! write_prolog_script(+Stream:stream, +Changes:list) is det.
%
%  Write a series of prolog terms indicating the changes in the list
%  =Changes= to the stream =Stream=.
write_prolog_script(Stream, Changes) :-
    maplist(change_script, Changes, Scripts),
    maplist({Stream}/[C]>>format(Stream, "~k.~n", [C]),
            Scripts).

changes_script([Change|Changes], [Script|Scripts]) :-
    once(change_script(Change, Script)),
    changes_script(Changes, Scripts).
changes_script([], []).

change_script(remove(Args, _Ctx), delete(Args)).
change_script(output(Args, Ctx), output(Args, Ctx.redirects)).
change_script(create(Files, _), create(Files)).
change_script(edited(_Files-DiffsAssoc, _Ctx), apply_diffs(Diffs)) :-
    assoc_to_list(DiffsAssoc, Diffs).
change_script(Change, Change).
