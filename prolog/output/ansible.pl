:- module(ansible, [write_ansible_playbook/2]).
/** <module> Ansible output

Output command history as a Ansible playbook

@author James Cash
*/

:- use_module(library(apply), [maplist/3, convlist/3, exclude/3]).
:- use_module(library(apply_macros)).
:- use_module(library(assoc), [assoc_to_list/2]).
:- use_module(library(debug), [debug/3]).
:- use_module(library(filesex), [directory_file_path/3]).
:- use_module(library(uuid), [uuid/1]).
:- use_module(library(yaml), [yaml_write/2]).
:- use_module(shell, [redirects_suffix/2]).

%! write_ansible_playbook(+Stream, +Changes) is det.
write_ansible_playbook(Stream, Changes) :-
    changes_yaml(Changes, Yaml),
    format(Stream, "---\n", []),
    yaml_write(Stream, Yaml).

%! changes_yaml(+Changes:list, -YamlChanges:list(dict)) is det.
%
%  Given a list of =Changes= (from processing.pl), create a list of
%  the corresponding dictionaries to be outputted as YAML.
changes_yaml(Changes, YamlChanges) :-
    maplist([Change, Yaml]>>(
                change_yaml(Change, Yaml0),
                arg(2, Change, Ctx),
                ctx_becomes(Ctx, Becomes),
                put_dict(Becomes, Yaml0, Yaml)
            ), Changes, YamlChanges).

%! change_yaml(+Change, -Yaml) is det.
%
%  Given a single change =Change=, =Yaml= is a dict representing the
%  corresponding Ansible command.
change_yaml(create(Args, Ctx), Yaml) :-
    file_change_yaml(present, Args, Ctx, Yaml).
change_yaml(remove(Args, Ctx), Yaml) :-
    file_change_yaml(absent, Args, Ctx, Yaml).
change_yaml(output(Cmd, Ctx), _{name: "command to file",
                                shell: _{cmd: CmdStr,
                                         chdir: Dir}}) :-
    Dir = Ctx.cwd,
    redirects_suffix(Ctx.redirects, Suffix),
    atomic_list_concat(Cmd, ' ', CmdStr0),
    format(string(CmdStr), "~w~w", [CmdStr0, Suffix]).
change_yaml(edited(Files-DiffsAssoc, _Ctx), _{name: Name, block: Block}) :-
    format(string(Name), "Patch files ~w", [Files]),
    assoc_to_list(DiffsAssoc, DiffsList),
    diffs_yaml(DiffsList, Block).
change_yaml(stream_edit(Args, Ctx), _{name: "TODO Stream edit"}).
change_yaml(firewall(Args, Ctx), _{name: "TODO ufw"}).
change_yaml(package_install(Packages, _Ctx), _{name: "packages install",
                                               apt: _{pkg: Packages}}).
change_yaml(system_control(Args), _{name: "TODO systemctl"}).
change_yaml(pipeline(Commands, Ctx), _{name: "TODO pipeline"}).

%! diffs_yaml(+FileDiffs:list(atom-string), -YamlBlocks:list(dict)) is det.
%
%  True when =FileDiffs= is a list of =File-Diff= terms (as created by
%  the =tracing= module) and =YamlBlocks= is a list of dicts,
%  representing the Ansible operations to replace the edits.
diffs_yaml([], []).
diffs_yaml([File-Diff|Diffs], [Block|Blocks]) :-
    format(string(BlockName), "patch ~w", [File]),
    uuid(RandSuffix),
    format(atom(PatchFile), "patch_file_~w", [RandSuffix]),
    format(atom(PatchFilePath), "{{ ~w.path }}", [PatchFile]),
    Block = _{name: BlockName,
              block: [_{name: "create temp file for patch",
                        tempfile: _{state: file, suffix: patch},
                        register: PatchFile},
                      _{name: "copy patch file",
                        copy: _{content: Diff,
                                dest: PatchFilePath}},
                      _{name: "apply diff",
                        patch: _{src: PatchFilePath,
                                 basedir: /,
                                 strip: 0}}]},
    diffs_yaml(Diffs, Blocks).

% Helpers

%! ctx_becomes(+Context:dict, -BecomeYaml:dict) is det.
%
%  Given the =Context=, give a dict specifying the "become" field, if
%  any.
ctx_becomes(Ctx, _{become: true}) :-
    _{as: "root"} :< Ctx, !.
ctx_becomes(Ctx, _{become: true, become_user: User}) :-
    _{as: User} :< Ctx, !.
ctx_becomes(_, _{}).

%! arg_files(+Args:list, +Cwd:string, -Files:list) is det.
%
%  True when =Files= is a list of the file arguments in =Args=,
%  expanded to absolute paths, relative to =Cwd=.
arg_files(Args, Cwd, Files) :-
    exclude(string_concat("-", _), Args, NonFlags),
    convlist(directory_file_path(Cwd), NonFlags, Files).

%! file_change_yaml(+State:atom, +Args:list, +Ctx:dict, -Yaml:dict) is det.
%
%  True when =Yaml= is a dictionary representing the Ansible changes
%  needed to apply the state =State= to the arguments =Args=, in the
%  context =Ctx=
file_change_yaml(State, Args, Ctx, Yaml) :-
    exclude(normalize_space(atom('')), Args, NonBlankArgs),
    arg_files(NonBlankArgs, Ctx.cwd, Files),
    files_yaml(State, Files, Ctx, Yaml).

%! files_yaml(+State:atom, +File:list, +Ctx:dict, -Yaml:dict) is det.
%
%  =Yaml= is ansible dict representing =Files= in =State=, given =Ctx=.
files_yaml(State, [File], _Ctx,
           _{file: _{path: File,
                     % [TODO] owner, etc from Ctx
                     state: State}}) :- !.
files_yaml(State, Files, _Ctx,
           _{file: _{path: "{{ item }}",
                     state: State},
             loop: Files}).
