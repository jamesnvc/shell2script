:- module(output, [write_script/3]).
/** <module> Output of history

Wrapper module for the different backends to output the command
history as repeatable scripts.

@author James Cash
@see output/shell.pl
@see output/prolog_terms.pl
*/

:- use_module(output/ansible, [write_ansible_playbook/2]).
:- use_module(output/prolog_terms, [write_prolog_script/2]).
:- use_module(output/shell, [write_shell_script/2]).

%! write_script(+Type:term, +Stream:stream, +Changes:list) is det.
%
%  Write an output script of type specified by =Type= that will
%  reproduce the changes given in =Changes= to the stream =Stream=.
%  =Changes= will be the "output" of processing:process_history/2.
write_script(prolog, Stream, Changes) :- write_prolog_script(Stream, Changes).
write_script(shell, Stream, Changes)  :- write_shell_script(Stream, Changes).
write_script(ansible, Stream, Changes) :- write_ansible_playbook(Stream, Changes).
