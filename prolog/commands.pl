:- module(commands, [line_command/3,
                     set_env_vars/3,
                     empty_env/1,
                     env_list/2,
                     env_get/3,
                     sh_string_val/3,
                     sh_string_val_quoted/3 ]).
/** <module> Parsing tokenized lines

A module for parsing the lines tokenized by the parsing module into
strings, setting the environment, expanding various shell
things & vars.

@author James Cash
@see parsing.pl
*/
:- use_module(library(apply), [maplist/3]).
:- use_module(library(assoc), [empty_assoc/1,
                               get_assoc/3,
                               put_assoc/4,
                               assoc_to_list/2]).
:- use_module(library(lists), [append/2]).
:- use_module(library(uid), [getuid/1,
                             user_data/3,
                             user_info/2]).
:- use_module(library(yall)).

%! empty_env(-Env) is det.
%
%  Unify =Env= with a new, empty environment.
empty_env(Env) :- empty_assoc(Env).

user_home(HomeDir) :-
    getuid(Uid),
    user_info(Uid, UserData),
    user_data(home, UserData, HomeDir).

expand_tilde, [ExStr] -->
    [S],
    { string_concat("~/", Suf, S), !,
      user_home(HomeDir),
      atomics_to_string([HomeDir, Suf], "/", ExStr) }.
expand_tilde, [S] --> [S].

expand -->
    expand_tilde.

sh_string_val_quoted(Env, V, S) :-
    sh_string_val_quoted_(V, Env, S).

sh_string_val_quoted_(s(Cs), _, S) :-
    format(string(S), "'~s'", [Cs]).
sh_string_val_quoted_(u(Cs), Env, S) :-
   maplist(lookup(Env), Cs, Expanded),
   append(Expanded, Codes),
   string_codes(S0, Codes),
   call_dcg(expand, [S0], [S]).
sh_string_val_quoted_(d(Cs), Env, S) :-
    maplist(lookup(Env), Cs, Expanded),
    append(Expanded, Codes),
    string_codes(S0, Codes),
    call_dcg(expand, [S0], [S1]),
    format(string(S), "\"~w\"", [S1]).

%! sh_string_val(+Env, +StrToken, -String) is semidet.
%
%  Given the tokenized representation of a shell string =StrToken=,
%  unify =String= with the corresponding string, after expanding
%  various shell things & evaluating variable substitutions.
%
%  @see parsing:line//2
sh_string_val(_, s(Cs), S) :-
    string_codes(S, Cs), !.
sh_string_val(Env, ShStr, S) :-
    ( ShStr = u(Cs) ; ShStr = d(Cs) ), !,
    maplist(lookup(Env), Cs, Expanded),
    append(Expanded, Codes),
    string_codes(S0, Codes),
    call_dcg(expand, [S0], [S]).

lookup(Env, var(Vcs), Val) :- !,
    string_codes(Var, Vcs),
    once( ( get_assoc(Var, Env, ValStr),
            string_codes(ValStr, Val) )
        ; Val = [] ).
lookup(_, C, [C]).

%! set_env_vars(+EnvVars:list, +EnvIn, -EnvOut) is det.
%
%  True when =EnvOut= is the result of applying the variable settings
%  in =EnvVars=, starting with the initial var bindings =EnvIn=.
set_env_vars([var_set(Vc, Sc)|Vs], E0, E2) :- !,
    string_codes(Var, Vc),
    sh_string_val(E0, Sc, Val),
    put_assoc(Var, E0, Val, E1),
    set_env_vars(Vs, E1, E2).
set_env_vars([], E, E).

%! line_command(+Env:assoc, +ShellLine:list, -Commands:list) is semidet.
%
%  Parse the tokenized shell lines in =ShellLine= into a list of
%  expanded strings, =Commands=, with =Env= containing the set
%  environment variables.
%  =Commands= is two lists: The first is the list of commands suitable
%  for executing, while the second is the "raw" commands, including
%  quotes, suitable for saving to history.
line_command(Env, [S|Ws], [C|Cs]-[HC|HCs]) :-
    sh_string_val(Env, S, C), !,
    sh_string_val_quoted(Env, S, HC), !,
    line_command(Env, Ws, Cs-HCs).
line_command(_, [], []-[]).

%! env_list(+Env, -List) is det.
%
%  Unify =List= with the variable bindings in =Env= in a format
%  suitable for passing to process_create/3.
env_list(Env, Lst) :-
    assoc_to_list(Env, List),
    maplist([K-V,K=V]>>true, List, Lst).

%! env_get(+Key, +Env, -Value) is semidet.
%
%  True when =Key= is bound to =Value= in =Env=.
env_get(K, Env, V) :-
    get_assoc(K, Env, V).
