#!/usr/bin/env bash

OUTPUT_FILE=${1:-shell_out}
SWIPL="$HOME/.swivm/versions/v8.1.17/bin/swipl"

exec "${SWIPL}" -g executing:main -t halt executing.pl "${OUTPUT_FILE}"
